#!/usr/bin/python3

from sipmessage import *

#f = open('INVITE', 'r')
requests = ['INVITE','ACK','CANCEL','REGISTER','BYE','OPTIONS']

#message = f.read()


def parse_message(text):
    lines = text.splitlines()
    message = parse_start_line(lines[0]) 
    for line in lines[1:]:
        words = line.split(':',1)
        message.headers[words[0].strip()] = words[1].strip()
    return message

'''
parses the first line of a sip message, and returns a message subclass instance
'''
def parse_start_line(message):
    words = message.split()
    if words[0] in requests:
        temp = Request()
        temp.method = words[0]
        temp.request_uri = words[1]
        return temp
    elif words[0] == 'SIP/2.0':
        return Response()
    #else:  #raise an error here 
    #    return 'Unkown/Not Supported'
    
