
class Message(object):
    def __init__(self):
        self.SIP_Version = 'SIP/2.0'
        self.headers = {}
        self.header_order = []
    def print_message():
        pass
    def return_message():
        pass
    def return_buffer_message():
        pass

class Request(Message):
    def __init__(self):
        super().__init__()
        self.method = None
        self.request_uri = None
        self.header_order = ['Via','Max-Forwards','To','From','Call-ID','CSeq','Contact','Content-Type','Content-Length']

    def setMethod(self, method):
        self.method = method

    def setURI(self, uri):
        self.request_uri = uri

    def return_message(self):
        message = self.method + ' ' + self.request_uri + ' ' + self.SIP_Version + '\r\n'
        for item in self.headers:
            message = message + item + ': ' + self.headers[item] + '\r\n'
        return message

    def print_message(self):
        print(self.return_message())

    def return_buffer_message(self):
        return self.return_message().encode('ascii')

class Response(Message):
    pass

