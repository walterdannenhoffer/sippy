from sipmessage import *
import string
import random


def generate_call_id(length=10):
    rng = random.SystemRandom()
    call_id = ''
    for i in range(length):
        call_id = call_id + rng.choice(string.ascii_letters+string.digits)
    return call_id

def generate_request(method, uri):
    new = Request()
    new.method = method
    new.request_uri = uri
    return new.return_buffer_message() 

def generate_invite(uri, to, from_header, cseq, via, call_id=generate_call_id(), max_forwards=70):
    new = generate_request('INVITE', uri)
